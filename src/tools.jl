using LaTeXStrings
using Printf
using FortranFiles
using Interpolations
using FFTW
using StatsBase
using CairoMakie

t = Theme(ScatterLines=(cycle = Cycle([:color, :marker], covary=true),),Axis=(xlabelsize=20,ylabelsize=20,))
set_theme!(t)

struct OmegaData{T<:Number}
    ky::Vector{T}
    γ::Vector{T}
    ω::Vector{T}
    OmegaData(n::Int) = new{Float64}(zeros(n),zeros(n),zeros(n))
end
function Base.setindex!(a::OmegaData, v::Vector, I)
    a.ky[I] = v[1]
    a.γ[I] = v[2]
    a.ω[I] = v[3]
end

function Base.getindex(a::OmegaData, I)
    return (a.ky[I], a.γ[I], a.ω[I])
end

function Base.iterate(x::OmegaData)
    return iterate(x, 1)
end

function Base.iterate(x::OmegaData, state::Int)
    if state == length(x.ky)+1
        return nothing
    end
    d = x[state]
    state += 1
    return d, state
end

struct NrgData{T<:Number}
    t::Vector{T}
    n₁::Matrix{T}
    u₁::Matrix{T}
    T₁::Matrix{T}
    T₁perp::Matrix{T}
    Γₑₛ::Matrix{T}
    Γₑₘ::Matrix{T}
    Qₑₛ::Matrix{T}
    Qₑₘ::Matrix{T}
    Πₑₛ::Matrix{T}
    Πₑₘ::Matrix{T}
    # NrgData(n_t::Int, n_species::Int) = new{Float64}(zeros(n_t),zeros(n_t, n_species),zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species), zeros(n_t, n_species))
end
# function Base.setindex!(a::NrgData, v::Vector, I, J)
#     a.n₁[I,J] = v[1]
#     a.u₁[I,J] = v[2]
#     a.T₁[I,J] = v[3]
#     a.T₁perp[I,J] = v[4]
#     a.Γₑₛ[I,J] = v[5]
#     a.Γₑₘ[I,J] = v[6]
#     a.Qₑₛ[I,J] = v[7]
#     a.Qₑₘ[I,J] = v[8]
#     a.Πₑₛ[I,J] = v[9]
#     a.Πₑₘ[I,J] = v[10]
# end

mutable struct MomData
    t::Vector{Float64}
    data::Array
    MomData(n_timesteps, nx0, nky0, nz0) = new(zeros(n_timesteps), zeros(Complex{Float64}, n_timesteps, nx0, nky0, nz0))
end

mutable struct FieldData
    t::Vector{Float64}
    data::Array
    FieldData(n_timesteps, nx0, nky0, nz0) = new(zeros(n_timesteps), zeros(Complex{Float64}, n_timesteps, nx0, nky0, nz0))
end

mutable struct ExtendedFieldData
    t::Vector{Float64}
    data::Array
    ExtendedFieldData(n_timesteps, nky0, nz0, n_domains) = new(zeros(n_timesteps), zeros(Complex{Float64}, n_timesteps, nky0, nz0*n_domains))
end



function read_n_from_last(io, n::Int)
    old = position(io)
    seekend(io)
    seek(io, position(io) - 2)
    for i ∈ 1:n
        seek(io, position(io) - 1)
        while Char(peek(io)) != '\n'
            seek(io, position(io) - 1)
        end
    end
    read(io, Char)
    r = readline(io)
    seek(io, old)
    return r
end

# Only 1d scans
function get_n_runs(path::String = ".")
    scan_log_path = joinpath(path, "scan.log")
    if !isfile(scan_log_path)
        throw("No scan.log file found in $path")
    end

    n_runs = open(scan_log_path) do f
        scan_lines = readlines(f)
        if length(scan_lines) <= 1
            throw("No lines found in scan.log")
        end
        parse(Int,split(scan_lines[end], "|")[1])
    end

    return n_runs
end

function get_omega(path::String = ".")
    n_runs = get_n_runs(path)

    omega_data = OmegaData(n_runs)

    for i ∈ 1:n_runs
        n_run = lpad(i,4,"0")
        omega_path = joinpath(path, "omega_$n_run")
        if !isfile(omega_path)
            throw("Missing file: $omega_path")
        end

        omega_data[i] = open(omega_path) do f
            omega_line = readline(f)
            parse.(Float64, split(omega_line, " ", keepempty=false))
        end
    end
    return omega_data
end

function plot_omega(path::String = ".", title::String = "")
    o = get_omega(path)

    f = Figure(figure_padding=(2, 6, 1, 6),)
    ax = Axis(f[1,1], xlabel=L"k_y \rho_s", ylabel=L"c_s/L_{ref}", title=title)
    xlims!(ax, 0,2.1)
    ylims!(ax, -1,.5)
    hlines!(ax, [0], color=:black, label="")
    scatterlines(ax, o.ky, o.γ, label="Growth rate", marker=:diamond)
    scatterlines(ax, o.ky, o.ω, label="Frequency", marker=:circle)
    return f
end

function plot_γ!(ax, path::String, s::Float64)
    label = @sprintf "s = %.2f" s
    o = get_omega(path)
    ky_list = Vector{Union{Missing,Float64}}()
    γ_list = Vector{Union{Missing,Float64}}()
    ω_list = Vector{Union{Missing,Float64}}()
    for (ky, γ, ω) ∈ o
        if ky == 1.7 && s == 0.7
            push!(ky_list, ky)
            push!(γ_list, missing)
            push!(ω_list, missing)
        end
        if γ != 0
            push!(ky_list, ky)
            push!(γ_list, γ)
            push!(ω_list, ω)
        elseif length(ky_list) > 0
            push!(ky_list, ky)
            push!(γ_list, missing)
            push!(ω_list, missing)
        end
    end
    scatterlines!(ax, ky_list, γ_list, label=label)
end

function plot_multiple_γ(path_list::Vector{String}, s_list::Vector{Float64})
    f = Figure(figure_padding=(2, 6, 1, 6),)
    ax = Axis(f[1,1], xlabel=L"k_y \rho_s", ylabel=L"\gamma/(c_s/L_{ref})")
    xlims!(ax, 0,2.0)
    # ylims!(ax, 0, 0.5)
    plot_multiple_γ!(ax, path_list, s_list)
    hlines!(ax, [0], color=:black)
    axislegend(ax, position=:lt)
    return f
end

function plot_multiple_γ!(ax, path_list::Vector{String}, s_list::Vector{Float64})
    # marker_list = [:square, :diamond, :circle, :utriangle, :dtriangle, :pentagon, :hexagon, :heptagon, :octagon]
    # color_list = palette(:default)
    for (path, s) ∈ zip(path_list, s_list)
        plot_γ!(ax, path, s)
    end
end

function plot_ω!(ax, path::String, s::Float64)
    label = @sprintf "s = %.2f" s
    o = get_omega(path)
    ky_list = Vector{Union{Missing,Float64}}()
    γ_list = Vector{Union{Missing,Float64}}()
    ω_list = Vector{Union{Missing,Float64}}()
    for (ky, γ, ω) ∈ o
        if ky == 1.7 && s == 0.7
            push!(ky_list, ky)
            push!(γ_list, missing)
            push!(ω_list, missing)
        end
        if γ != 0
            push!(ky_list, ky)
            push!(γ_list, γ)
            push!(ω_list, ω)
        elseif length(ky_list) > 0
            push!(ky_list, ky)
            push!(γ_list, missing)
            push!(ω_list, missing)
        end
    end
    scatterlines!(ax, ky_list, ω_list, label=label)
end

function plot_multiple_ω(path_list::Vector{String}, s_list::Vector{Float64})
    f = Figure(figure_padding=(2, 6, 1, 6),)
    ax = Axis(f[1,1], xlabel=L"k_y \rho_s", ylabel=L"\omega/(c_s/L_{ref})")
    xlims!(ax, 0,2.0)
    ylims!(ax, -1,0)
    plot_multiple_ω!(ax, path_list, s_list)
    axislegend(ax, position=:lt)
    return f
end

function plot_multiple_ω!(ax, path_list::Vector{String}, s_list::Vector{Float64})
    for (path, s) ∈ zip(path_list, s_list)
        plot_ω!(ax, path, s)
    end
end

function get_nrg(;path::String = ".", extension::String = ".dat")
    p = read_parameters(path=path, extension=extension)
    n_species = p.n_spec
    t = Vector{Float64}()
    n₁ = Vector{Float64}()
    u₁ = Vector{Float64}()
    T₁ = Vector{Float64}()
    T₁perp = Vector{Float64}()
    Γₑₛ = Vector{Float64}()
    Γₑₘ = Vector{Float64}()
    Qₑₛ = Vector{Float64}()
    Qₑₘ = Vector{Float64}()
    Πₑₛ = Vector{Float64}()
    Πₑₘ = Vector{Float64}()
    open(joinpath(path,"nrg"*extension)) do f
        while !eof(f)
            line = readline(f)
            push!(t, parse(Float64, line))
            for j ∈ 1:n_species
                line = readline(f)
                data = parse.(Float64, split(line, " ", keepempty=false))
                push!(n₁, data[1])
                push!(u₁, data[2])
                push!(T₁, data[3])
                push!(T₁perp, data[4])
                push!(Γₑₛ, data[5])
                push!(Γₑₘ, data[6])
                push!(Qₑₛ, data[7])
                push!(Qₑₘ, data[8])
                push!(Πₑₛ, data[9])
                push!(Πₑₘ, data[10])
            end
        end
    end
    n_timesteps = length(t)
    return NrgData(
        t,
        reshape(n₁, (n_species,n_timesteps)),
        reshape(u₁, (n_species,n_timesteps)),
        reshape(T₁, (n_species,n_timesteps)),
        reshape(T₁perp, (n_species,n_timesteps)),
        reshape(Γₑₛ, (n_species,n_timesteps)),
        reshape(Γₑₘ, (n_species,n_timesteps)),
        reshape(Qₑₛ, (n_species,n_timesteps)),
        reshape(Qₑₘ, (n_species,n_timesteps)),
        reshape(Πₑₛ, (n_species,n_timesteps)),
        reshape(Πₑₘ, (n_species,n_timesteps))
    ), p.nonlinear
end

function plot_nrg!(ax; path::String = ".", extension::String = ".dat", species = 1, label=missing)
    if ismissing(label)
        label = extension
    end
    nrg, nonlinear = get_nrg(path=path, extension=extension)

    if nonlinear
        lines!(ax, nrg.t, nrg.Qₑₛ[species,:], label=label)
    else
        lines!(ax, nrg.t, nrg.n₁[species,:].^2, label=label)
    end
    # plot!(nrg.t, nrg.u₁[:,species], label=L"<|u_1^2|>")
    # plot!(nrg.t, nrg.T₁[:,species], label=L"<|T_1^2|>")
    # plot!(nrg.t, nrg.T₁perp[:,species], label=L"<|T_{1\perp}^2|>")
    return ax
end

function plot_nrg(;path::String = ".", extension::String = ".dat", species = 1, label=missing)
    if ismissing(label)
        label = extension
    end
    nrg, nonlinear = get_nrg(path=path, extension=extension)

    if nonlinear
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"t", ylabel=L"<|Q_{es}|>")
        xlims!(ax, 0,nrg.t[end])
        ylims!(ax, 0,maximum(nrg.Qₑₛ[species,:])*1.1)
        lines!(ax, nrg.t, nrg.Qₑₛ[species,:], label=label)
    else
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"t", ylabel=L"<|n_{1}|^2>", xscale=log10, yscale=log10)
        xlims!(ax, 0,nrg.t[end])
        ylims!(ax, 0,maximum(nrg.n₁[species,:].^2)*1.1)
        lines!(ax, nrg.t, nrg.n₁[species,:].^2, label=label)
    end
    # plot!(nrg.t, nrg.u₁[:,species], label=L"<|u_1^2|>")
    # plot!(nrg.t, nrg.T₁[:,species], label=L"<|T_1^2|>")
    # plot!(nrg.t, nrg.T₁perp[:,species], label=L"<|T_{1\perp}^2|>")
    return f
end

function plot_nrgs(;path::String = ".", extensions::Array{String} = [".dat"], species = 1, label=missing)
    if ismissing(label)
        label = extensions[1]
    end
    nrg, set_nonlinear = get_nrg(path=path, extension=extensions[1])

    x = Vector{Float64}()
    y = Vector{Float64}()

    for extension in extensions
        nrg, nonlinear = get_nrg(path=path, extension=extension)
        if nonlinear != set_nonlinear
            throw("All components need to be either linerar or nonlinear")
        end
        append!(x, nrg.t)
        if nonlinear
            append!(y, nrg.Qₑₛ[species,:])
        else
            append!(y, nrg.n₁[species,:].^2)
        end
    end

    if set_nonlinear
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"t", ylabel=L"<|Q_{es}|>")
        xlims!(ax, 0,nrg.t[end])
    else
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"t", ylabel=L"<|n_{1}|^2>", xscale=log10, yscale=log10)
        xlims!(ax, 0,nrg.t[end])
    end
    lines!(ax, x, y, label=label)

    # plot!(nrg.t, nrg.u₁[:,species], label=L"<|u_1^2|>")
    # plot!(nrg.t, nrg.T₁[:,species], label=L"<|T_1^2|>")
    # plot!(nrg.t, nrg.T₁perp[:,species], label=L"<|T_{1\perp}^2|>")
    return f
end

function plot_nrgs!(ax, ;path::String = ".", extensions::Array{String} = [".dat"], species = 1, label=missing)
    if ismissing(label)
        label = extensions[1]
    end
    nrg, set_nonlinear = get_nrg(path=path, extension=extensions[1])

    x = Vector{Float64}()
    y = Vector{Float64}()

    for extension in extensions
        nrg, nonlinear = get_nrg(path=path, extension=extension)
        if nonlinear != set_nonlinear
            throw("All components need to be either linerar or nonlinear")
        end
        append!(x, nrg.t)
        if nonlinear
            append!(y, nrg.Qₑₛ[species,:])
        else
            append!(y, nrg.n₁[species,:].^2)
        end
    end

    lines!(ax, x, y, label=label)

    # plot!(nrg.t, nrg.u₁[:,species], label=L"<|u_1^2|>")
    # plot!(nrg.t, nrg.T₁[:,species], label=L"<|T_1^2|>")
    # plot!(nrg.t, nrg.T₁perp[:,species], label=L"<|T_{1\perp}^2|>")
    return ax
end

function check_convergence(path1::String = ".", path2::String = "."; max_diff::Float64 = 0.03)
    omega1 = get_omega(path1)
    omega2 = get_omega(path2)

    overlapping1 = findall(in(omega2.ky),omega1.ky)
    overlapping2 = findall(in(omega1.ky),omega2.ky)

    for (i1,i2) ∈ zip(overlapping1, overlapping2)
        rgammadiff = (omega1.γ[i1] - omega2.γ[i2])/omega1.γ[i1]*100
        romegadiff = (omega1.ω[i1] - omega2.ω[i2])/omega1.ω[i1]*100

        @printf "%.2f:\n Δγ = %.2f%%\n Δω = %.2f%%\n" omega1.ky[i1] rgammadiff romegadiff
    end
end

function get_mean(;path::String = ".", extension::String = ".dat", t=0, species = 1)
    nrg, nonlinear = get_nrg(path=path, extension=extension)

    f(x) = x >= t

    n_t = findfirst(f, nrg.t)

    return mean(nrg.Qₑₛ[species, n_t:end]), std(nrg.Qₑₛ[species, n_t:end])
end

function get_means(;path::String = ".", extensions::Array{String} = [".dat"], t=0, species = 1)
    nrg, set_nonlinear = get_nrg(path=path, extension=extensions[1])

    x = Vector{Float64}()
    y = Vector{Float64}()

    for extension in extensions
        nrg, nonlinear = get_nrg(path=path, extension=extension)
        if nonlinear != set_nonlinear
            throw("All components need to be either linerar or nonlinear")
        end
        append!(x, nrg.t)
        if nonlinear
            append!(y, nrg.Qₑₛ[species,:])
        else
            append!(y, nrg.n₁[species,:].^2)
        end
    end

    f(x_) = x_ >= t

    n_t = findfirst(f, x)

    return mean(y[n_t:end]), std(y[n_t:end])
end

function get_mom(;path::String = ".", extension::String = ".dat", species = "ions")
    p = read_parameters(path=path, extension=extension)

    n_timesteps = p.number_of_computed_time_steps÷p.istep_mom + 2

    f = FortranFile(joinpath(path, "mom_"*species*extension))
    momData = [MomData(n_timesteps, p.nx0, p.nky0, p.nz0) for i ∈ 1:p.n_moms]
    for i ∈ 1:n_timesteps
        t = read(f, Float64)
        for f_num ∈ 1:p.n_moms
            momData[f_num].t[i] = t
            momData[f_num].data[i,:,:,:] = read(f, (ComplexF64, (p.nx0, p.nky0, p.nz0)))
        end
    end
    return momData
end

function get_field(;path::String = ".", extension::String = ".dat")
    p = read_parameters(path=path, extension=extension)

    n_timesteps = p.number_of_computed_time_steps÷p.istep_field + 2

    f = FortranFile(joinpath(path, "field"*extension))
    fieldData = [FieldData(n_timesteps, p.nx0, p.nky0, p.nz0) for i ∈ 1:p.n_fields]
    for i ∈ 1:n_timesteps
        t = read(f, Float64)
        for f_num ∈ 1:p.n_fields
            fieldData[f_num].t[i] = t
            fieldData[f_num].data[i,:,:,:] = read(f, (ComplexF64, (p.nx0, p.nky0, p.nz0)))
        end
    end
    return fieldData
end

mutable struct GeneParameters
    n_moms::Int
    n_fields::Int
    nx0::Int
    nz0::Int
    nky0::Int
    kymin::Float64
    n_pol::Int
    istep_field::Int
    istep_mom::Int
    istep_nrg::Int
    n_spec::Int
    step_time::Float64
    dt_vlasov::Float64
    number_of_computed_time_steps::Int
    shat::Float64
    lx::Float64
    ly::Float64
    adapt_lx::Bool
    n0_global::Int
    ky0_ind::Int
    q0::Float64
    nonlinear::Bool
    GeneParameters() = new(0,0,0,0,0,0.0,1,0,0,0,0,0.0,0.0,0,0.0,0.0,0.0, true, 0, 1, 0.0, false)
end

function read_parameters(;path::String = ".", extension::String = ".dat")
    p = GeneParameters()
    parameter_symbols = fieldnames(GeneParameters)
    for line ∈ eachline(joinpath(path, "parameters"*extension))
        line_without_comments = split(line, "!")[1]

        if !occursin('=', line_without_comments)
            continue
        end

        line_pieces = split(line_without_comments, "=")
        var_name = replace(strip(line_pieces[1]), " " => "_")
        value = strip(line_pieces[2])
        for (symbol, symbol_name) ∈ zip(parameter_symbols, string.(parameter_symbols))
            if var_name == symbol_name
                var_type = typeof(getproperty(p, symbol))
                parsed_value = if (var_type == Bool)
                    value == "T" ? true : false
                else
                    parse(var_type, value)
                end
                setproperty!(p, symbol, parsed_value)
            end
        end
    end
    return p
end


## FieldData ben: 1 = kx, 2 = ky,
function extend_field_data(parameters::GeneParameters, fieldData::FieldData)
    n_pol = max(1,parameters.n_pol)
    n_domains = parameters.nx0 - iseven(parameters.nx0)
    n_timesteps = length(fieldData.t)

    nx_mod = n_domains÷2

    num_2pinpol_from_tube = [-nx_mod:nx_mod;]
    nexc = sign(parameters.shat) * (1 - parameters.adapt_lx * (1 - abs(parameters.shat) * parameters.kymin * parameters.lx * n_pol))
    kx_prime_idx = vcat([parameters.nx0-nx_mod+1:parameters.nx0;],[1:nx_mod+1;])

    if sign(parameters.shat) == -1
        reverse!(kx_prime_idx)
    end

    extendedFieldData = ExtendedFieldData(n_timesteps, parameters.nky0, parameters.nz0, n_domains)

    extendedFieldData.t = fieldData.t

    shift = nexc
    phasefac = complex(-1.0)^shift*exp(2π*im*parameters.n_pol*parameters.n0_global*parameters.q0)
    # phasefac = (-1.0+im*0.0)^nexc
    fright = conj(phasefac).^abs.(range(1,nx_mod))
    fleft = phasefac.^abs.(range(-nx_mod,-1))

    aux_jbar_fac = vcat(fleft,[1],fright)
    for i ∈ 1:n_domains
        extendedFieldData.data[:,:,(i-1)*parameters.nz0+1:i*parameters.nz0] =  aux_jbar_fac[i]  .* fieldData.data[:,kx_prime_idx[i],:,:]
    end
    return extendedFieldData
end

function get_ballooning_data(parameters::GeneParameters, fieldData::FieldData, t_idx::Int64=-1, kyidx::Int64=1)
    extendedFieldData = extend_field_data(parameters, fieldData)
    data = if t_idx == -1.0
        extendedFieldData.data[end,kyidx, :]
    else
        extendedFieldData.data[t_idx,kyidx, :]
    end

    max_z = parameters.nx0 - iseven(parameters.nx0)
    nz_ext = max_z * parameters.nz0
    dz = 2 * max_z / nz_ext
    z = max(parameters.n_pol,1).*(-max_z:dz:(max_z-dz))

    return z,data
end

function plot_ballooning_data(;path::String = ".", extension::String = ".dat", field::Symbol=:Φ, log=false)
    field_idx = map_field_idx(field)
    return plot_ballooning_data(path, extension, field_idx, log=log)
end

function plot_ballooning_data(path::String, extension::String, field_idx::Int64; log=false)
    parameters = read_parameters(path=path, extension=extension)
    fields = get_field(path=path, extension=extension)

    x, y = get_ballooning_data(parameters, fields[field_idx])

    maxy = maximum(abs.(y))
    miny = minimum(abs.(y))
    minx = minimum(x)
    maxx = maximum(x)
    if log
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"\theta_p/\pi", ylabel=L"\Phi", yscale=log10)
        xlims!(ax, minx, maxx)
        ylims!(ax, miny*0.1, maxy*5)
    else
        f = Figure(figure_padding=(2, 6, 1, 6),)
        ax = Axis(f[1,1],xlabel=L"\theta_p/\pi", ylabel=L"\Phi")
        xlims!(ax, minx, maxx)
    end
    lines!(ax, x, abs.(y), label=L"|\Phi|")
    lines!(ax, x, real.(y), label=L"\Re(\Phi)")
    lines!(ax, x, imag.(y), label=L"\Im(\Phi)")
    axislegend(ax, position=:lt)
    return f
end

function resample(y::Vector, n)
    n_old = length(y)
    x = range(0,1,n_old)
    interp = linear_interpolation(x,y)
    return interp(range(0,1,n))
end

function phases(fieldData::FieldData, momData::MomData)
    f_fft = ifft(fieldData.data[end,:,:,:], [1])[:]

    m_fft = ifft(momData.data[end,:,:,:], [1])[:]

    frac = f_fft./m_fft

    return angle.(frac)
end

function map_field_idx(field::Symbol)
    if field == :Φ
        return 1
    elseif field == :A
        return 2
    end
    return -1
end

function map_mom_idx(mom::Symbol)
    if mom == :n || mom == :nₚ
        return 1
    elseif mom == :Tpar || mom == :Tparₚ
        return 2
    elseif mom == :Tperp || mom == :Tperpₚ
        return 3
    elseif mom == :qpar || mom == :qparₚ
        return 4
    elseif mom == :qperp || mom == :qperpₚ
        return 5
    elseif mom == :upar || mom == :uparₚ
        return 6
    elseif mom == :nₜ
        return 7
    elseif mom == :Tparₜ
        return 8
    elseif mom == :Tperpₜ
        return 9
    elseif mom == :qparₜ
        return 10
    elseif mom == :qperpₜ
        return 11
    elseif mom == :uparₜ
        return 12
    end
    return -1
end

function map_field_str(field::Symbol)
    if field == :Φ
        return L"\Phi"
    elseif field == :A
        return L"A"
    end
    return L"error"
end

function map_mom_str(mom::Symbol)
    if mom == :n
        return L"n"
    elseif mom == :nₚ
        return L"n_p"
    elseif mom == :Tpar
        return L"T_{\parallel}"
    elseif mom == :Tparₚ
        return L"T_{\parallel,p}"
    elseif mom == :Tperp
        return L"T_{\perp}"
    elseif mom == :Tperpₚ
        return L"T_{\perp,p}"
    elseif mom == :qpar
        return L"q_{\parallel}"
    elseif mom == :qparₚ
        return L"q_{\parallel,p}"
    elseif mom == :qperp
        return L"q_{\perp}"
    elseif mom == :qperpₚ
        return L"q_{\perp,p}"
    elseif mom == :upar
        return L"u_{\parallel}"
    elseif mom == :uparₚ
        return L"u_{\parallel,p}"
    elseif mom == :nₜ
        return L"n_t"
    elseif mom == :Tparₜ
        return L"T_{\parallel,t}"
    elseif mom == :Tperpₜ
        return L"T_{\perp,t}"
    elseif mom == :qparₜ
        return L"q_{\parallel,t}"
    elseif mom == :qperpₜ
        return L"q_{\perp,t}"
    elseif mom == :uparₜ
        return L"u_{\parallel,t}"
    end
    return L"error"
end

function plot_phase(;path::String = ".", extension::String = ".dat", species::String = "ions", field::Symbol=:Φ, mom::Symbol=:n)
    field_idx = map_field_idx(field)
    mom_idx = map_mom_idx(mom)
    label = latexstring(map_field_str(field), L"\times", map_mom_str(mom))
    return plot_phase_(path, extension, species, field_idx, mom_idx, label)
end

function plot_phase!(ax;path::String = ".", extension::String = ".dat", species::String = "ions", field::Symbol=:Φ, mom::Symbol=:n)
    field_idx = map_field_idx(field)
    mom_idx = map_mom_idx(mom)
    label = latexstring(map_field_str(field), L"\times", map_mom_str(mom))
    return plot_phase_!(ax, path, extension, species, field_idx, mom_idx, label)
end

format_π_fracs(xs; denom=round(Int, π/minimum(diff(xs)))) = map(xs) do x
    # This will error if x is not an expected fraction of π
    frac = Int(round(x/(π/denom), digits=1))//denom
    sign = x < 0 ? "-" : ""
    absnum = abs(frac.num) == 1 ? "" : string(abs(frac.num))
    frac.den == 1 && return frac.num == 0 ? L"0" : L"%$sign%$(absnum)\pi"
    return L"%$sign\frac{%$(absnum)\pi}{%$(frac.den)}"
end

function plot_phase_!(ax, path::String = ".", extension::String = ".dat", species = "ions", field_idx::Int=1, mom_idx::Int=1, label = "", weight = true)
    parameters = read_parameters(path=path, extension=extension)
    fieldData = get_field(path=path, extension=extension)
    println(fieldData[field_idx].t[end])
    momData = get_mom(path=path, extension=extension, species=species)
    println(momData[mom_idx].t[end])

    angle = phases(fieldData[field_idx], momData[mom_idx])
    display(angle)

    binsize = 0.1
    nbins = Int(ceil(2pi/binsize))
    h = if weight

        f_fft = conj(fft(conj(fieldData[field_idx].data[end,:,:,:]), [1]))[:]

        m_fft = conj(fft(conj(momData[mom_idx].data[end,:,:,:]), [1]))[:]

        abstempdata1 = abs.(f_fft)
        abstempdata2 = abs.(m_fft)

        absval = abstempdata1./sum(sum(abstempdata1, dims=3), dims=1) .* abstempdata2./sum(sum(abstempdata2, dims=3), dims=1)
        # println(absval)

        fit(Histogram, angle, weights(absval), (-pi):binsize:(-pi+nbins*binsize))
    else
        fit(Histogram, angle, (-pi):binsize:(-pi+nbins*binsize), closed=:left)
    end
    # x = (collect(h.edges[1]).+binsize/2)[1:end-1]
    x = range(-π,π,nbins)
    y = h.weights./(parameters.nx0*parameters.nz0)

    xlims!(ax, -π,π)
    ylims!(ax, low=0)
    lines!(ax, x,y, label = label)
end

function plot_phase_(path::String = ".", extension::String = ".dat", species = "ions", field_idx::Int=1, mom_idx::Int=1, label = "", weight = true)
    f = Figure(figure_padding=(2, 6, 1, 6))
    ax = Axis(f[1,1],xlabel=L"\alpha", ylabel=L"Arb. Units",xticks = (-π:π/2:π, ["π", "-π/2", "0", "π/2", "π"]),title = species)

    plot_phase_!(ax, path, extension, species, field_idx, mom_idx, label, weight)

    return f
end

function meshgrid(x, y)
    X = [i for i in x, j in 1:length(y)]
    Y = [j for i in 1:length(x), j in y]
    return X, Y
end


function plot_contours(;path::String = ".", extension::String = ".dat", field::Symbol=:Φ)
    parameters = read_parameters(path=path, extension=extension)
    field_idx = map_field_idx(field)
    mom_idx = map_mom_idx(field)

    b = if field_idx != -1
        f = get_field(path=path, extension=extension)[field_idx]
        t = f.data[end,:,:,parameters.nz0÷2]
        real.(conj(fft(conj(t))))
    else mom_idx != -1
        f = get_mom(path=path, extension=extension)[mom_idx]
        t = f.data[end,:,:,parameters.nz0÷2]
        real.(conj(fft(conj(t))))
    end

    z = b'

    xmax = parameters.lx / 2
    x = collect(range(-xmax, xmax, parameters.nx0))
    ymax = parameters.ly / 2
    y = collect(range(-ymax, ymax, parameters.nky0))
    print(size(x), size(y), size(z))

    f = Figure(figure_padding=(2, 6, 1, 6),resolution = (1080, 1080), fontsize=24)
    ax = Axis(f[1, 1], aspect=xmax/ymax, xlabel=L"x/\rho_s", ylabel=L"y/\rho_s", ylabelsize=32, xlabelsize=32)
    xlims!(ax,-xmax,xmax)
    ylims!(ax,-ymax,ymax)
    c = contourf!(ax, x,y,b, levels=20,colormap=:turbo,aspect_ratio = xmax/ymax)
    # c = heatmap!(ax, x, y, b, colormap=:turbo, interpolate=true)
    Colorbar(f[1, 2], c,label = L"\Phi", labelsize=32)
    return f
end

