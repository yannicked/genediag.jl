using GeneTools
using CairoMakie
using MakiePublication

function plot_ball()
    for s ∈ [60, 65, 70, 75, 80]
        f = plot_ballooning_data(path="redo/pIdent", extension="_$s")
        save("plots2/ball/0.$s.svg", f)
        save("plots2/ball/0.$s.eps", f)
        display(f)
    end
end

lc = Cycle([:color, :linestyle], covary=true)
with_theme(plot_ball, theme_aps(width=3.5, linecycle=lc))
