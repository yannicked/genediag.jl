using GeneTools
using CairoMakie
using MakiePublication

function plot_phases_trapped()
  ex = "_80"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.80.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.80.svg", f)
  display(f)

  ex = "_75"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.75.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.75.svg", f)
  display(f)

  ex = "_70"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.70.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.70.svg", f)
  display(f)

  ex = "_65"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.65.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.65.svg", f)
  display(f)

  ex = "_60"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.60.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.60.svg", f)
  display(f)

  ex = "_0011"
  f = GeneTools.plot_phase(path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.40.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.40.svg", f)
  display(f)

  ex = "_0008"
  f = GeneTools.plot_phase(path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.30.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.30.svg", f)
  display(f)

  ex = "_0006"
  f = GeneTools.plot_phase(path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_trapped_0.20.eps", f)
  save("plots2/phases/phases_electrons_trapped_0.20.svg", f)
  display(f)

  ex = "_80"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.80.eps", f)
  save("plots2/phases/phases_ions_trapped_0.80.svg", f)
  display(f)

  ex = "_75"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.75.eps", f)
  save("plots2/phases/phases_ions_trapped_0.75.svg", f)
  display(f)

  ex = "_70"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.70.eps", f)
  save("plots2/phases/phases_ions_trapped_0.70.svg", f)
  display(f)

  ex = "_65"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.65.eps", f)
  save("plots2/phases/phases_ions_trapped_0.65.svg", f)
  display(f)

  ex = "_60"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.60.eps", f)
  save("plots2/phases/phases_ions_trapped_0.60.svg", f)
  display(f)

  ex = "_0011"
  f = GeneTools.plot_phase(path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.40.eps", f)
  save("plots2/phases/phases_ions_trapped_0.40.svg", f)
  display(f)

  ex = "_0008"
  f = GeneTools.plot_phase(path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.30.eps", f)
  save("plots2/phases/phases_ions_trapped_0.30.svg", f)
  display(f)

  ex = "_0006"
  f = GeneTools.plot_phase(path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:nₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:Tparₜ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₜ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_trapped_0.20.eps", f)
  save("plots2/phases/phases_ions_trapped_0.20.svg", f)
  display(f)
end

function plot_phases_passing()
  ex = "_80"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.80.eps", f)
  save("plots2/phases/phases_electrons_passing_0.80.svg", f)
  display(f)

  ex = "_75"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.75.eps", f)
  save("plots2/phases/phases_electrons_passing_0.75.svg", f)
  display(f)

  ex = "_70"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.70.eps", f)
  save("plots2/phases/phases_electrons_passing_0.70.svg", f)
  display(f)

  ex = "_65"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.65.eps", f)
  save("plots2/phases/phases_electrons_passing_0.65.svg", f)
  display(f)

  ex = "_60"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.60.eps", f)
  save("plots2/phases/phases_electrons_passing_0.60.svg", f)
  display(f)

  ex = "_0011"
  f = GeneTools.plot_phase(path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.40.eps", f)
  save("plots2/phases/phases_electrons_passing_0.40.svg", f)
  display(f)

  ex = "_0008"
  f = GeneTools.plot_phase(path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.30.eps", f)
  save("plots2/phases/phases_electrons_passing_0.30.svg", f)
  display(f)

  ex = "_0006" # ky=0.8
  f = GeneTools.plot_phase(path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "electrons", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_electrons_passing_0.20.eps", f)
  save("plots2/phases/phases_electrons_passing_0.20.svg", f)
  display(f)

  ex = "_80"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.80.eps", f)
  save("plots2/phases/phases_ions_passing_0.80.svg", f)
  display(f)

  ex = "_75"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.75.eps", f)
  save("plots2/phases/phases_ions_passing_0.75.svg", f)
  display(f)

  ex = "_70" # ky=0.7
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.70.eps", f)
  save("plots2/phases/phases_ions_passing_0.70.svg", f)
  display(f)

  ex = "_65" # ky=0.8
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.65.eps", f)
  save("plots2/phases/phases_ions_passing_0.65.svg", f)
  display(f)

  ex = "_60"
  f = GeneTools.plot_phase(path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/pIdent/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.60.eps", f)
  save("plots2/phases/phases_ions_passing_0.60.svg", f)
  display(f)

  ex = "_0011" # ky=1.3
  f = GeneTools.plot_phase(path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.4/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.40.eps", f)
  save("plots2/phases/phases_ions_passing_0.40.svg", f)
  display(f)

  ex = "_0008" # ky=1.0
  f = GeneTools.plot_phase(path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.3/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.30.eps", f)
  save("plots2/phases/phases_ions_passing_0.30.svg", f)
  display(f)

  ex = "_0006" # ky=0.8
  f = GeneTools.plot_phase(path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:nₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:Tparₚ)
  GeneTools.plot_phase!(f.current_axis.x, path = "redo/s0.2/", extension = ex, species = "ions", field=:Φ, mom=:Tperpₚ)
  
  axislegend(f.current_axis.x, position=:rt)

  save("plots2/phases/phases_ions_passing_0.20.eps", f)
  save("plots2/phases/phases_ions_passing_0.20.svg", f)
  display(f)
end

lc = Cycle([:color, :linestyle], covary=true)
with_theme(plot_phases_trapped, theme_aps(width=4.5, linecycle=lc))
with_theme(plot_phases_passing, theme_aps(width=4.5, linecycle=lc))